using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace FidusApi.Entities
{
    public class Customer : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        [EmailAddress]
        public string Email { get; set; }
        public string Cellphone { get; set; }
        public string VatID { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressPostcode { get; set; }
        public string AddressLocation { get; set; }

        public List<Transaction> Transactions { get; set; }
    }
}