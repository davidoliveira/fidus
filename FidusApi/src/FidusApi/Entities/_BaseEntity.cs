using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace FidusApi.Entities
{
    public abstract class BaseEntity
    {        
        public int ID { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}