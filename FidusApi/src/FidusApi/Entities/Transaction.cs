using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;

namespace FidusApi.Entities
{
    public class Transaction : BaseEntity
    {        
        public string CouponID { get; set; }
        public decimal Amount { get; set; }
        public int Balance { get; set; }

        public int CustomerID { get; set; }
        public Customer Customer { get; set; }
    }
}