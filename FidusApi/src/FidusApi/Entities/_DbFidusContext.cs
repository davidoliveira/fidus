using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;

namespace FidusApi.Entities
{
    public class DbFidusContext : DbContext
    {
        public DbFidusContext() : base()
        { 
            
        }

        public DbFidusContext(DbContextOptions<DbFidusContext> options) : base(options)
        { 

        }


        public DbSet<Customer> Customers { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.HasDefaultSchema("api");
            modelBuilder.Entity<Customer>().HasKey(b => b.ID);
            modelBuilder.Entity<Customer>().Property(b => b.ID).ValueGeneratedOnAdd();

            modelBuilder.Entity<Transaction>().HasKey(b => b.ID);
            modelBuilder.Entity<Transaction>().Property(b => b.ID).ValueGeneratedOnAdd();
            modelBuilder.Entity<Transaction>().HasKey(b => b.CustomerID);
            modelBuilder.Entity<Transaction>().HasOne(p => p.Customer).WithMany(b => b.Transactions).HasForeignKey(p => p.CustomerID); 
            //https://docs.efproject.net/en/latest/modeling/relationships.html
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer("server=cp76.webserver.pt;database=fidusdb;uid=fidususr;password=Q94f7bk#;");
        }
    }

    public static class DbInitializer
    {
        public static void Initialize(DbFidusContext context)
        {
            context.Database.EnsureCreated();                        
            //context.Database.Migrate();

            /*
            dotnet ef migrations add Initials
            dotnet ef database update
            https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/migrations
            */
        }
    }
}