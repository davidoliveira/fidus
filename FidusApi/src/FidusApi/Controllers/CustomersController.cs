using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FidusApi.Entities;
using FidusApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FidusApi.Controllers
{
    // public abstract class EntityController<C, T> : Controller
    // {
    //     private readonly RepoBase<C, T> repoEntity;

    //     public EntityController(RepoBase<T> repo)
    //     {
    //         this.repoEntity = repo;            
            
    //     }
    // }

    //[Authorize]
    [Route("[controller]")]
    public class CustomersController : Controller
    {
        private readonly IRepoCustomer repoCustomer;

        public CustomersController(IRepoCustomer repoCustomer)
        {
            this.repoCustomer = repoCustomer;
        }

        [HttpGet]
		public HttpResponseModel<IEnumerable<Customer>> GetAll([FromQuery]int pageId, [FromQuery]int pageSize, [FromQuery]string searchValue)
        {           
			return new HttpResponseModel<IEnumerable<Customer>>() { Data = this.repoCustomer.GetAll() };
        }

        [HttpGet("{id}")]
        public HttpResponseModel<Customer> GetSingle(int id)
        {            
			return new HttpResponseModel<Customer>() { Data = this.repoCustomer.GetSingle(id) };
        }

        [HttpPost]
        public HttpResponseModel<Customer> Create([FromBody]Customer item)
        {            
            item.DateCreated = DateTime.Now;
            this.repoCustomer.Add(item);
            this.repoCustomer.Save();
            return new HttpResponseModel<Customer>() { Data = item };
        }

        [HttpPut("{id}")]
        public HttpResponseModel<Customer> Update(int id, [FromBody]Customer item)
        {            
            item.DateUpdated = DateTime.Now;   
			this.repoCustomer.Add(item);
            this.repoCustomer.Update(item);
            this.repoCustomer.Save();
            return new HttpResponseModel<Customer>() { Data = item };
        }

        [HttpDelete("{id}")]
        public HttpResponseModel<Customer> Delete(int id)
        {
            var item = this.repoCustomer.GetSingle(id);
            this.repoCustomer.Delete(item);
            return new HttpResponseModel<Customer>() { Data = item };
        }
    }
}
