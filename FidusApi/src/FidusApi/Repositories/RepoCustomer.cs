using System;
using System.Collections.Generic;
using FidusApi.Entities;
using Microsoft.EntityFrameworkCore;

namespace FidusApi.Repositories
{
  public interface IRepoCustomer : IRepo<Customer>
  {

  }
  public class RepoCustomer : RepoBase<DbFidusContext, Customer>, IRepoCustomer
  {
      public RepoCustomer(DbFidusContext dbContext) : base(dbContext)
      {
          
      }
  }
}