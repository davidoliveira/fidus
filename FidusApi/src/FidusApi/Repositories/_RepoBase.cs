using System;
using System.Collections.Generic;
using FidusApi.Entities;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FidusApi.Repositories
{
  public interface IRepo<T> where T : class
  {
      void Add(T entity);    
      void Update(T entity);
      void Save();
      
      void Delete(T entity);
      T GetSingle(int id);
      IEnumerable<T> GetAll();
  }

  // http://www.tugberkugurlu.com/archive/generic-repository-pattern-entity-framework-asp-net-mvc-and-unit-testing-triangle

  public abstract class RepoBase<C, T> : IRepo<T> where T : BaseEntity where C : DbContext, new()
  {
      private C db = new C();
      public C Context 
      {
        get { return db; }
        set { db = value; }
      }

      public RepoBase(C dbContext)
      {
          this.db = dbContext;
      }

      public T GetSingle(int id)
      {
        return db.Set<T>().Single(b => b.ID == id);
      }
      public virtual IEnumerable<T> GetAll() 
      {          
          return db.Set<T>().ToList();
      }

      public virtual void Add(T entity) 
      {
        db.Set<T>().Add(entity);
      }

      public virtual void Update(T entity) 
      {
        db.Entry(entity).State = EntityState.Modified;
      }

      public virtual void Save()
      {        
        db.SaveChanges();
      }

      public virtual void Delete(T entity)
      {
        db.Set<T>().Remove(entity);
      }
  }
}