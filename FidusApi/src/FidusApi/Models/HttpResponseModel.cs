using Newtonsoft.Json;

public class HttpResponseModel<T>
{
    public HttpResponseModel()
    {
        this.Code = 200;
        this.Message = "Success";
    }

    public int Code {get;set;}
    public string Message { get; set; }

    public T Data {get;set;}        

    public override string ToString()
    {
        return JsonConvert.SerializeObject(this);
    }
}