﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using FidusApi.Entities;
using FidusApi.Repositories;
using System.Text;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System;

namespace FidusApi
{
	public class Startup
	{
		public Startup(IHostingEnvironment env)
		{
			var builder = new ConfigurationBuilder()
				.SetBasePath(env.ContentRootPath)
				.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
				.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
				.AddEnvironmentVariables();
			Configuration = builder.Build();
		}

		public IConfigurationRoot Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<DbFidusContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

			services.AddCors();

			// Add framework services.
			services.AddMvc().AddJsonOptions(options =>
			{
				options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
			});

			// https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection
			services.AddScoped<IRepoCustomer, RepoCustomer>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, DbFidusContext dbContext)
		{
			loggerFactory.AddConsole(Configuration.GetSection("Logging"));
			loggerFactory.AddDebug();

			app.UseExceptionHandler(errorApp =>
			{
				errorApp.Run(async context =>
				{
					context.Response.StatusCode = 200;
					context.Response.ContentType = "application/json";

					var error = context.Features.Get<IExceptionHandlerFeature>();
					if (error != null)
					{
						var ex = error.Error;
						while (ex.InnerException != null)
							ex = ex.InnerException;

						await context.Response.WriteAsync(new HttpResponseModel<String>
						{
							Code = 200,
							Message = ex.Message,
							Data = null
						}.ToString(), Encoding.UTF8);
					}
				});
			});

			app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials());

			app.UseMvc();


			// https://stormpath.com/blog/token-authentication-asp-net-core
			// var secretKey = "mysupersecret_secretkey!123";
			// var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));

			// var tokenValidationParameters = new TokenValidationParameters
			// {
			//     // The signing key must match!
			//     ValidateIssuerSigningKey = true,
			//     IssuerSigningKey = signingKey,

			//     // Validate the JWT Issuer (iss) claim
			//     ValidateIssuer = true,
			//     ValidIssuer = "ExampleIssuer",

			//     // Validate the JWT Audience (aud) claim
			//     ValidateAudience = true,
			//     ValidAudience = "ExampleAudience",

			//     // Validate the token expiry
			//     ValidateLifetime = true,

			//     // If you want to allow a certain amount of clock drift, set that here:
			//     ClockSkew = TimeSpan.Zero
			// };

			// app.UseJwtBearerAuthentication(new JwtBearerOptions
			// {
			//     AutomaticAuthenticate = true,
			//     AutomaticChallenge = true,
			//     TokenValidationParameters = tokenValidationParameters
			// });

			// Migrate the database
			Entities.DbInitializer.Initialize(dbContext);
		}
	}
}
