import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

import { CustomerService } from './../shared/customer.service';

import { Account } from './../shared/Account';
import { Customer } from './../shared/Customer';
import { Transaction } from './../shared/Transaction';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
  public customers: Customer[] = null;
  public pageId: number = 1;
  public pageSize: number = 25;
  public searchValue: string = null;

  constructor(public customerService: CustomerService, public router: Router) 
  { 

  }

  ngOnInit() {
    let _self = this;

    _self.customerService.getAll(_self.pageId, _self.pageSize, _self.searchValue).subscribe((response: any) => {
      console.log(response);
    });
  }

}
