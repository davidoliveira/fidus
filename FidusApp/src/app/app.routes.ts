import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderComponent } from './order/order.component';
import { CustomerComponent } from './customer/customer.component';
import { CustomersComponent } from './customers/customers.component';

const appRoutes: Routes = [
    { path: '', component: OrderComponent, pathMatch: 'full' },
    { path: 'order', component: OrderComponent },
    { path: 'order/:id', component: OrderComponent },
    { path: 'customers', component: CustomersComponent },
    { path: 'customer', component: CustomerComponent },
    { path: 'customer/:id', component: CustomerComponent }
];

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);