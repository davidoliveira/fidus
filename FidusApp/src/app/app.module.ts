import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// import { AUTH_PROVIDERS } from 'angular2-jwt';
// import { AlertModule } from 'ng2-bootstrap';

import './rxjs-extensions';
import { routing, appRoutingProviders } from './app.routes';

import { CustomerService } from './shared/customer.service';

import { AppComponent } from './app.component';
import { CustomerComponent } from './customer/customer.component';
import { OrderComponent } from './order/order.component';
import { CustomersComponent } from './customers/customers.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomerComponent,
    OrderComponent,
    CustomersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [appRoutingProviders, CustomerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
