import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

import { CustomerService } from './../shared/customer.service';

import { Account } from './../shared/Account';
import { Customer } from './../shared/Customer';
import { Transaction } from './../shared/Transaction';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  public order: Order;

  constructor(public customerService: CustomerService, public router: Router) { 
    this.order = new Order();
    
  }

  ngOnInit() {
    
  }

  submit() {
    let _self = this;
    
    let customer = new Customer();
    //customer.AccountID = 1;
    customer.ID = 1;
    customer.Name = 'David Oliveira'
    customer.DateCreated = new Date();

    let transaction = new Transaction();
    transaction.ID = 42435;
    transaction.CouponID = '123';
    transaction.CustomerID = 1;
    transaction.Amount = 23.67;
    transaction.Balance = 24;
    transaction.DateCreated = new Date();  

    customer.Transactions = new Array();
    customer.Transactions.push(transaction);
    
    _self.customerService.save(customer).subscribe(() => {
      
    });
  }
}

export class Order 
{ 
    CustomerID: string;
    CouponID: string;    
    Amount: number;
}