import { Injectable } from '@angular/core';
import { Http, Response, Request, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
//import { AuthHttp } from 'angular2-jwt';

import { Customer } from './../shared/customer';

@Injectable()
export class CustomerService {  
  private DB_BASE_URI: string = 'http://localhost:5000';

  private headers: Headers;

  constructor(/*public authHttp: AuthHttp,*/ public http: Http) { 
    this.headers = new Headers();
    //this.headers.append('Authorization', 'type=master&ver=1.0&sig=5mDuQBYA0kb70WDJoTUzSBMTG3owkC0/cEN4fqa18/s=');
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  save(customer: Customer): Observable<any>
  { 
    let _self = this;     
    let options = new RequestOptions({ headers: _self.headers });   
    return _self.http
      .post(_self.DB_BASE_URI + '/customers', JSON.stringify(customer), options)
      .map((r: Response) => r.json())
      .map((data:any) => { 
        
        return data;
      })
    ;
  }

  getAll(pageId: number, pageNumber: number, searchValue: string): Observable<any>
  { 
    let _self = this;     
    let options = new RequestOptions({ headers: _self.headers });   
    return _self.http
      .get(_self.DB_BASE_URI + '/customers/?pageId=&pageSize=&searchValue=', options)
      .map((r: Response) => r.json())
      .map((data:any) => { 
        
        return data;
      })
    ;
  }

  remove(customer: Customer) 
  { 
    let uri = this.DB_BASE_URI;
    
  }

}
