import { Customer } from './customer';

export class Transaction 
{ 
    ID: number;
    DateCreated: Date;
    DateUpdated: Date;
    
    CouponID: string;    
    Amount: number;
    Balance: number;

    CustomerID: number;
    Customer: Customer;    
}