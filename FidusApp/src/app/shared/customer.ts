import { Transaction } from './transaction';

export class Customer 
{ 
    ID: number;
    DateCreated: Date;
    DateUpdated: Date;
    
    Name: string;
    Email: string;
    Cellphone: string;
    VatID: string;
    AddressLine1: string;
    AddressLine2: string;
    AddressPostcode: string;
    AddressLocation: string;    

    Transactions: Transaction[];
}