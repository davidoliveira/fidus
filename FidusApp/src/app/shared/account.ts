import { Customer } from './customer';

export class Account 
{ 
    ID: number;
    DateCreated: Date;
    DateUpdated: Date;

    Name: string;

    Customers: Customer[];   
}