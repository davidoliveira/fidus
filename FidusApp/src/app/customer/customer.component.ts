import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

import { CustomerService } from './../shared/customer.service';

import { Account } from './../shared/Account';
import { Customer } from './../shared/Customer';
import { Transaction } from './../shared/Transaction';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  public customer: Customer;
  public transaction: Transaction;

  constructor(public customerService: CustomerService, public router: Router) { 
    this.customer = new Customer();
    this.transaction = new Transaction();
  }

  ngOnInit() {

  }

  submit() {
    let _self = this;

    _self.customerService.save(_self.customer).subscribe(() => {
      
    });
  }

}
